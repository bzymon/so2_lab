#include<linux/module.h>
#include<linux/random.h>
#include<linux/slab.h>
#include<linux/radix-tree.h>

#define UPPER_LIMIT 65535

static RADIX_TREE(root,GFP_KERNEL);
static char *string_arr[UPPER_LIMIT] = {};

static int __init radixtreemod_init(void)
{
	long int i; 
	int *random_number=NULL;
	char *data=NULL;
	for(i=1;i<UPPER_LIMIT;i<<=3) {
		random_number = (char*)kmalloc(sizeof(char), GFP_KERNEL);
		if(IS_ERR(random_number)) {
			pr_alert("Error allocating memory: %ld\n",PTR_ERR(random_number));
			return -ENOMEM;
		}
		get_random_bytes(random_number,sizeof(int));
		*random_number%=2017;
		if(radix_tree_insert(&root,i,(void *)string_arr[counter]))
			pr_alert("Error inserting item to radix tree!\n");
		counter++;
	}

	for(i=1;i<UPPER_LIMIT;i<<=3) {
		data = (char *)radix_tree_lookup(&root,i);
		if(data)
			pr_notice("Value retrieved from radix tree: %s for index: %ld\n",data,i);
		else
			pr_alert("Error retrieving data from tree!\n");
	}

	return 0;
}

static void __exit radixtreemod_exit(void)
{
	char *data=NULL;
	long int i;

	for(i=1;i<UPPER_LIMIT;i<<=3) {
		data  = (char *)radix_tree_delete(&root,i);
		if(data) {
			pr_notice("Value retrieved from radix tree: %d for index: %ld\n",*data,i);
			kfree(data);
		} else
			pr_alert("Error retrieving data from tree!\n");
	}
}

module_init(radixtreemod_init);
module_exit(radixtreemod_exit);

MODULE_AUTHOR("Arkadiusz Chrobot <a.chrobot@tu.kielce.pl>");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("A module that demonstrates the usage of kernel radix tree implementation.");
MODULE_VERSION("1.0");

