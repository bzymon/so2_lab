#include <linux/module.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/radix-tree.h>

#define NUMBER_OF_ELEMENTS 10
#define UPPER_LIMIT 65535

static RADIX_TREE(root, GFP_KERNEL);

static char *string_arr[NUMBER_OF_ELEMENTS] = {};
static int number_of_elements = 0;
static int counter = 0;

module_param_array(string_arr, charp, &number_of_elements, 0644);
MODULE_PARM_DESC(string_arr, "ARRAY CHAR");

static int __init radixtreemod_init(void)
{
        long int i;
        char *data = NULL;
        char *str = NULL;
        for(i=1; i<65535; i<<=3) {
                str = (char*)kmalloc(sizeof(char), GFP_KERNEL);
                if(IS_ERR(str)) {
                        pr_alert("Error alocating memory\n");
                        return -ENOMEM;
                }
                if(radix_tree_insert(&root, i, (void *)string_arr[counter]))
                        pr_alert("Error inserting item to radix tree!\n");
                counter++;
        }
        for(i=1; i<65535; i<<=3) {
                data = (char*)radix_tree_lookup(&root, i);
                if(data)
                        pr_notice("Value retrieved from radix tree: %s for index %ld\n", data, i);
                else
                        pr_alert("Error retrieving data from tree!\n");
        }
        return 0;
}

static void __exit radixtreemod_exit(void)
{
        char *data = NULL;
        long int i;
        pr_info("Deleting radix tree!\n");
        for(i=1; i<65535; i<<=3) {
                data = (char *)radix_tree_delete(&root, i);
                if(data) {
                        pr_notice("Value retrieved from radix tree: %s for index: %ld\n", data, i);
                        kfree(data);
                } else
                        pr_alert("Error retrieving data from tree!\n");
        }
}

module_init(radixtreemod_init);
module_exit(radixtreemod_exit);

MODULE_LICENSE("GPL");


