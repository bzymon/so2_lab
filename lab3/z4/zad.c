#include <linux/module.h>
#include <linux/kfifo.h>
#include <linux/string.h>

#define NUMBER_OF_ELEMENTS 10

static struct kfifo fifo_queue;

static int counter = 0;
static char *string_arr[NUMBER_OF_ELEMENTS] = { "Jeden", "Dwa", "Trzy" };
static int number_of_elements = 3;

module_param_array(string_arr, charp, &number_of_elements, 0644);
MODULE_PARM_DESC(string_arr, "array parameter");

static int __init fifo_init(void)
{
	int returned_value;
	unsigned int returned_size;

	returned_value = kfifo_alloc(&fifo_queue, number_of_elements*sizeof(char*), GFP_KERNEL);
	if(returned_value) {
		pr_alert("Error allocating kfifo!\n");
		return -ENOMEM;
	}
	while(!kfifo_is_full(&fifo_queue)) {
		if(counter == number_of_elements)
			break;
		returned_size = kfifo_in(&fifo_queue, &string_arr[counter], sizeof(char*));
		if(returned_size != sizeof(char*))
			pr_alert("ERROR!\n");
		else
			pr_notice("Enqueue: %s\n", string_arr[counter]);
		counter++;
	}
	return 0;
}


static void __exit fifo_exit(void)
{
	int returned_size;
	char *str;

	while(!kfifo_is_empty(&fifo_queue)) {
		returned_size = kfifo_out(&fifo_queue, &str, sizeof(&string_arr));
		if(returned_size != sizeof(&string_arr))
			pr_alert("DEQUEUE ERROR!\n");
		else
			pr_notice("Value from the fifo dequeue: %s\n", str);
	}
	kfifo_free(&fifo_queue);
	pr_notice("FIFO queue is empty.\n");

}

module_init(fifo_init);
module_exit(fifo_exit);

MODULE_LICENSE("GPL");
