#include<linux/module.h>
#include<linux/sched.h>
#include<linux/sched/signal.h>

static int __init init_tasklist(void)
{
	struct task_struct *p;

	for_each_process(p) {
		
		if(strcmp(p->comm, "program")==0){
		printk(KERN_INFO "Process name is: %s, and its pid is: %i. ",p->comm, p->pid);
		if(p->state==TASK_RUNNING)
			printk("Process state is TASK_RUNNING\n");
		if(p->state==TASK_INTERRUPTIBLE)
			printk("Process state is TASK_INTERRUPTIBLE\n");
		if(p->state==TASK_UNINTERRUPTIBLE)
			printk("Process state is TASK_UNINTERRUPTIBLE\n");
		if(p->state==TASK_STOPPED)
			printk("Proces state is TASK_STOPPED\n");
		}
	}
	return 0;
}

static void __exit exit_tasklist(void) {

	struct task_struct *p;
	
	for_each_process(p) {
		if(strcmp(p->comm, "program")==0){
		printk(KERN_INFO "Process name is: %s, and its pid is: %i. ",p->comm, p->pid);
		if(p->state==TASK_RUNNING)
			printk("Process state is TASK_RUNNING.\n");
		if(p->state==TASK_INTERRUPTIBLE)
			printk("Process state is TASK_INTERRUPTIBLE.\n");
		if(p->state==TASK_UNINTERRUPTIBLE)
			printk("Process state is TASK_UNINTERRUPTIBLE.\n");
		if(p->state==TASK_STOPPED)
			printk("Proces state is TASK_STOPPED.\n");
	}
	}
}

module_init(init_tasklist);
module_exit(exit_tasklist);

MODULE_LICENSE("GPL");
