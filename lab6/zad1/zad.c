#include<linux/module.h>
#include<linux/interrupt.h>

static void normal_tasklet_handler(unsigned long int data)
{
	pr_info("Hi! I'm a tasklet of a normal priority. My ID is: %lu\n",data);
}

static void privileged_tasklet_handler(unsigned long int data)
{
	pr_info("Hi! I'm a tasklet of a high priority. My ID is: %lu\n", data);
}

static DECLARE_TASKLET_DISABLED(normal_tasklet_1,normal_tasklet_handler,0);
static DECLARE_TASKLET(normal_tasklet_2,normal_tasklet_handler,1);
static DECLARE_TASKLET(privileged_tasklet_1,privileged_tasklet_handler,0);
static DECLARE_TASKLET(privileged_tasklet_2,privileged_tasklet_handler,1);


static int __init tasklets_init(void)
{
	printk("\n");
	tasklet_enable(&normal_tasklet_1);
	tasklet_disable(&normal_tasklet_2);
	tasklet_enable(&normal_tasklet_2);
	tasklet_disable_nosync(&privileged_tasklet_1);
	tasklet_enable(&privileged_tasklet_1);
	tasklet_schedule(&normal_tasklet_1);
	tasklet_schedule(&normal_tasklet_2);
	tasklet_hi_schedule(&privileged_tasklet_1);
	tasklet_hi_schedule(&privileged_tasklet_2);
	return 0;
}

static void __exit tasklets_exit(void)
{
	tasklet_kill(&normal_tasklet_1);
	tasklet_kill(&normal_tasklet_2);
	tasklet_kill(&privileged_tasklet_1);
	tasklet_kill(&privileged_tasklet_2);
}

module_init(tasklets_init);
module_exit(tasklets_exit);
MODULE_LICENSE("GPL");

