#include<linux/module.h>
#include<linux/kthread.h>
#include<linux/wait.h>
#include<linux/types.h>

static int __init threads_init(void)
{
	int num = 0;
	int k,l,m;
	printk("\n");
	printk("Poczatkowa wartosc: %d\n", num);

	set_bit(0, (void*)&num);
	printk("Wartosc po set_bit: %d\n", num);
	
	clear_bit(0, (void*)&num);
	printk("Wartosc po clear_bit: %d\n", num);
	
	change_bit(0, (void*)&num);
	printk("Wartosc po change_bit: %d\n", num);
	
	k = test_and_clear_bit(0, (void*)&num);
	printk("Wartosc po test_and_clear_bit: %d\nTest_and_clear_bit zwrocilo: %d\n", num, k);
	
	l = test_and_set_bit(0, (void*)&num);
	printk("Wartosc po test_and_set_bit: %d\nTest_and_set_bit zwrocilo: %d\n", num, l);
	
	m = test_and_change_bit(0, (void*)&num);
	printk("Wartosc po test_and_change_bit: %d\nTest_and_change_bit zwrocilo: %d\n", num, m);
	
	return 0;
}

static void __exit threads_exit(void)
{

}


module_init(threads_init);
module_exit(threads_exit);

MODULE_LICENSE("GPL");
