#include<linux/module.h>

static int __init first_init(void)
{
	printk(KERN_ALERT"TESTEREK\n");
	return 0;
}

static void __exit first_exit(void)
{
	printk(KERN_ALERT"Good bye\n");
}

module_init(first_init);
module_exit(first_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Arkadiusz Chrobot <a.chrobot@tu.kielce.pl>");
MODULE_DESCRIPTION("Another \"Hello World!\" kernel module :-)");
MODULE_VERSION("1.0");

