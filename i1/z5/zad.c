
#include<linux/module.h>

static int __init first_init(void)
{
	double x = 0.1 * 0.1;
	printk(KERN_ALERT"This is line %d of file \"%s\".\n",
            __LINE__, __FILE__);
	return 0;
}

static void __exit first_exit(void)
{
	printk(KERN_ALERT"Good bye\n");
}

module_init(first_init);
module_exit(first_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Szymon Baran");
MODULE_DESCRIPTION("zad2");
MODULE_VERSION("1.0");

