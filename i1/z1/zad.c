#include<linux/module.h>

static int __init first_init(void)
{
	u8 a = 77;
	u64 b = 8123;
	printk(KERN_ALERT"To jest jedynka: %u, a to jest osemka: %u", sizeof(a), sizeof(b));
	return 0;
}



static void __exit first_exit(void)
{
	printk(KERN_ALERT"KONIEC\n");
}

module_init(first_init);
module_exit(first_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Szymon Baran");
MODULE_DESCRIPTION("zad1");
MODULE_VERSION("1.0");

