#include<linux/module.h>

static int __init first_init(void)
{
	if(__KERNEL__ == 1)
		printk(KERN_ALERT"Wlaczone do modulu jadra.");
	else if(__KERNEL__ == 0)
		printk(KERN_ALERT"Wlaczone do zwyklego programu.");
	return 0;
}

static void __exit first_exit(void)
{
	printk(KERN_ALERT"KONIEC 6\n");
}

module_init(first_init);
module_exit(first_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Szymon Baran");
MODULE_DESCRIPTION("zad6");
MODULE_VERSION("1.0");

